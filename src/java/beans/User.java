/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DALi
 */
public class User {
    private Integer id;
    private String first_name;
    private String last_name;
    private String user_name;
    private String password;
    private Integer user_group_id;
    private String email;
    private String address;
    private String phone;

    public User() {
    }
    
    public User(Integer id, String first_name, String last_name, String user_name, String password, Integer user_group_id, String email, String address, String phone) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.user_name = user_name;
        this.password = password;
        this.user_group_id = user_group_id;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }
    
    public User(Integer id, String first_name, String last_name, String user_name, Integer user_group_id, String email, String address, String phone) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.user_name = user_name;
        this.user_group_id = user_group_id;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(Integer user_group_id) {
        this.user_group_id = user_group_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    /**
     *
     * @param request
     * @return
     */
    public static int getUserGroupID(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        User u = (User)session.getAttribute("user");
        return u.getUser_group_id();
    }

}
