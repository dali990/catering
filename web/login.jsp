<%-- 
    Document   : login
    Created on : Jan 14, 2018, 5:51:23 PM
    Author     : DALi
--%>

<%@page import="beans.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carbon - Admin Template</title>
    <link rel="stylesheet" href="./vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="./vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="./css/styles.css">

</head>
<body>
<div class="page-wrapper flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <% String poruka = (String)request.getAttribute("poruka"); 
                   String error = (String)request.getAttribute("error"); 
                   User user = (User)request.getAttribute("user"); 
                if(poruka != null){
                %>
                    <div class="alert alert-dismissible alert-success">
                        <%=poruka%>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <% } 
                
                if(error != null){
                %>
                    <div class="alert alert-dismissible alert-danger">
                        <%=error%>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <% } %>
                
              
                    
                <c:choose>
                    <c:when test="<% session.getAttribute('user') != null ? true:false;%>">
                        <h3>Welcome </h3>
                    </c:when>    
                    <c:otherwise>
                        <h3>Not Logged in.</h3>
                    </c:otherwise>
                </c:choose>
                
                <div class="card p-4">
                    <div class="card-header text-center text-uppercase h4 font-weight-light">
                        Login
                    </div>
                    <form action="Logovanje" method="POST" id="logovanje">
                        <div class="card-body py-5">
                            <div class="form-group">
                                <label class="form-control-label">Username</label>
                                <input name="username" type="text" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label">Password</label>
                                <input name="password" type="password" class="form-control">
                            </div>

                            <div class="custom-control custom-checkbox mt-4">
                                <input type="checkbox" class="custom-control-input" id="login">
                                <label class="custom-control-label" for="login">Remember me</label>
                            </div>
                        </div>
                    

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-5">Login</button>
                                </div>

                                <div class="col-6">
                                    <a href="#" class="btn btn-link">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/popper.js/popper.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="./vendor/chart.js/chart.min.js"></script>
<script src="./js/carbon.js"></script>
<script src="./js/demo.js"></script>
<script src="./js/validate.js"></script>
<script src="./js/myValidation.js"></script>
</body>
</html>

