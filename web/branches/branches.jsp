<%-- 
    Document   : branches
    Created on : Sep 15, 2018, 5:19:46 PM
    Author     : DALi
--%>

<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="beans.Branch"%>

<%@ include file="../layouts/header.jsp" %>      
<%@ include file="../layouts/menu.jsp" %>
<%    
    ArrayList<Branch> branches = (ArrayList<Branch>) request.getAttribute("branches");
//        ArrayList<Branch> branches = new ArrayList<Branch>();
//        Branch br = Branch.find(1);
//        branches.add(br);
%> 
<div class="content">
    <%@ include file="../layouts/msg.jsp" %>
    <div class="container-fluid">

        <div class="card">
            <div class="card-header bg-light">
                Poslovnice
            </div>

            <div class="card-body">
                <% if (branches != null) { %>  
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Naziv</th>
                                <th>Adresa</th>
                                <th>Telefon</th>
                                <th>Sef poslovnice</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% for (Branch b : branches) {%>
                            <tr>
                                <td class="text-nowrap"><%=b.getName()%></td>
                                <td><%=b.getAddress()%></td>
                                <td><%=b.getPhone()%></td>
                                <td><%=b.getManager()%></td>
                                <td>
                                    <a href="Branches?edit=<%=b.getId()%>" class="btn btn-outline-secondary btn-sm">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="Branches?delete=<%=b.getId()%>"  class="btn btn-outline-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
                <% } else { %>
                No results
                <% }%>
                <hr>
                <a href="Branches?addNewBranch" class="btn btn-outline-primary btn-sm">
                    Nova poslovnica <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
    </div>
</div>

<%@ include file="../layouts/footer.jsp" %>
