<%-- 
    Document   : addNewBranch
    Created on : Sep 16, 2018, 2:47:36 PM
    Author     : DALi
--%>

<%@page import="beans.Branch"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="beans.User"%>
<%@page isELIgnored="false" %>

<%@ include file="../layouts/header.jsp" %>      
<%@ include file="../layouts/menu.jsp" %>

<%    
    Branch b = (Branch) request.getAttribute("branch");
    ArrayList<User> users = (ArrayList<User>) request.getAttribute("users");
%> 
<div class="content">
    <%@ include file="../layouts/msg.jsp" %>
    <div class="container-fluid">

        <div class="card">
            <div class="card-header bg-light">
                Unos nove poslovnice
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <form id="editBranch" action="Branches?insert" method="POST">
                            <div class="form-group">
                                <label for="normal-input" class="form-control-label">Naziv</label>
                                <input name="name" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="normal-input" class="form-control-label">Adresa</label>
                                <input name="address" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="normal-input" class="form-control-label">Telefon</label>
                                <input name="phone" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="managers" class="form-control-label">Odgovorni</label>
                                <select class="form-control" id="managers" name="manager_id">
                                    <option></option>
                                    <% for (User u : users) {%>
                                    <option value="<%=u.getId()%>" >
                                        <%=u.getFirst_name()+" "+u.getLast_name()%>
                                    </option>
                                    <% } %>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primaryl">Sacuvaj</button>
                            </div>
                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div>
</div>

<script src="./js/validate.js"></script>
<script>
    (function(){
        let V = new Validate("editBranch");
        V.setRules({
            name:{
                name: "Naziv",
                rule: "required|string|max:64",
                msg: {required:"Polje naziv je obavezno"}
            },
            address:{
                name: "Adresa",
                rule: "required|string|max:64"
            },
            phone:{
                name: "Telefon",
                rule: "required|string|min:9"
            },
            manager_id:{
                name: "Menadzer",
                rule: "required|number|max:3|min:1",
                msg: {number:"Nedozovljena vrednost"}
            }
        });
        V.run();
    })();
</script>

<%@ include file="../layouts/footer.jsp" %>

