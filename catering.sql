--
-- Database: `catering`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `manager_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `name`, `address`, `phone`, `manager_id`) VALUES
(1, 'Catering 1', 'Naziv ulice 1', '011/225225', 6),
(2, 'Catering 2', 'Naziv ulice 2', '011/226226', 4),
(3, 'Poslovnica 3', 'Adresa poslovnice 3', '011/229/229', 4);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `food_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(2) NOT NULL,
  `size` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `recipe` text COLLATE utf8_unicode_ci,
  `price` int(11) NOT NULL,
  `info` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `food_name`, `category`, `size`, `description`, `recipe`, `price`, `info`) VALUES
(1, 'Pizza', 1, NULL, 'Pizza capricciosa is a style of pizza in Italian cuisine prepared with mozzarella cheese, Italian baked ham, mushroom, artichoke and tomato.', '1.\nU manju posudu dodati svez kvasac,2 kafene kasicice secera,2kasike brasna i 2 dcl tople vode izmutiti dobro viljuskom i ostaviti desetak minuta\n\n2.\nStaviti brasno u vanglicu,dodati kasiku supenu soli,predhodno spremljen kvasac,preostala 3dcl tople vode,margarin i ulje i zamesiti testo.Testo dobro izraditi kad pocne da se odvaja od posude ostaviti oko 45min da naraste\n\n3.\nKada je testo naraslo,dobro ga premesiti na radnoj povrsini koja je predhodno posuta brasnom.Od testa otkinuti 5 loptica svaku izraditi pa nauljiti.Uzeti okruglu tepsiju precnika 28-30cm,nauljiti je i lopticu razvuci po tepsiji(jako je lako)\n\n4.\nPodlogu namazati parasajz sokom ili pireom koji je malo razmucen sa vodom,preko toga staviti sunku isecenu na velike kocke,zatim pecurke,narendati odozgo kackavalj(najvise koristim krstas ili neki drugi ali da na njemu pise da je sir od parenog testa)odozgo posuti malo origana i ukrasiti maslinama\n\n5.\nPeci u dobro zagrejanoj rerni na 220C 10 min', 1300, 'Capricciosa'),
(2, 'Svecani Bolonjez', 1, NULL, 'Our best ever spaghetti Bolognese is super easy and a true Italian classic with a meaty, chilli sauce.', '1.\r\nKažu Italijani da je osnova svega luk , celer i paradajz.....Puno luka iseckanog  dinstajte na maslinovom ulju,kod mene je hladno ceđeno,što zdravije,mediteran efekat čuva vaše srce..\r\n\r\nDodajte so da se fino otpuste sokovi iz povrća ,ja biber dodajem odmah malo i pred kraj..a vi kako volite..\r\n\r\n2.\r\nNa staklast luk udrobite mlevenu junetinu i fino dinstajte ,uz dodatak malo temeljca goveđeg,dodajte četiri decilitra ,pola litra,polako neka se sokovi mešaju,a onda biber,origano,tračak kurkume i naravno malo svežeg iseckanog paradajza...mešajte i dodajte tečnost po potrebi\r\n\r\n I naravno,paradajz pire da se krčka\r\n\r\n3.\r\nKada se dehidrira   ,dodajte dva decilitra crnog vina i lovor list,trunku žutog šećera i malu štapku celera,da otpušta ukus i miris...pazite na to,bolonjez voli mešanje i bdenje...\r\n\r\nah,ta ljubav svemu daje ukus..blue sastojak ))\r\n\r\n', 1700, 'Capricciosa');

-- --------------------------------------------------------

--
-- Table structure for table `food_material`
--

CREATE TABLE `food_material` (
  `food_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `measure` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `food_material`
--

INSERT INTO `food_material` (`food_id`, `material_id`, `measure`, `quantity`) VALUES
(1, 2, 'g', 200),
(1, 3, 'g', 250),
(1, 5, 'g', 400),
(1, 6, 'g', 200),
(2, 1, 'g', 300),
(2, 3, 'g', 300),
(2, 4, 'g', 400);

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`) VALUES
(1, 'So'),
(2, 'Šećer'),
(3, 'Suncokretovo uilje'),
(4, 'Maslinovo ulje'),
(5, 'Krompir'),
(6, 'Šargarepa'),
(7, 'Brašno'),
(8, 'Kvasac'),
(9, 'Origano'),
(10, 'Šunka'),
(11, 'Suva slanina'),
(12, 'Crna čokolada'),
(13, 'Bela Čokolada'),
(14, 'Jagode'),
(15, 'Mleko'),
(16, 'Jaja'),
(17, 'Orasi');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `ingredient_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `measure` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `ingredient_name`, `measure`, `quantity`, `branch_id`) VALUES
(1, 'meso', 'kg', 10, 0),
(2, 'sir', 'kg', 10, 0),
(3, 'paradajz', 'kg', 10, 0),
(4, 'spagete', 'kg', 10, 0),
(5, 'brasno', 'kg', 100, 0),
(6, 'sunka', 'kg', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `first_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `office` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `first_name`, `last_name`, `role`, `office`, `username`, `password`) VALUES
(1, 'Aleksandar', 'Obradovic', 'ad', '*', 'aca', 'aca'),
(2, 'Marko', 'Markovic', 'mn', '*', 'marko', 'marko'),
(3, 'Ivan', 'Ivanovic', 'sp', 'BG', 'ivan', 'ivan'),
(4, 'Milos', 'Milosevic', 'sk', 'BG', 'milos', 'milos'),
(5, 'Filip', 'Filipovic', 'sp', 'NS', 'filip', 'filip'),
(6, 'Janko', 'Jankovic', 'sk', 'NS', 'janko', 'janko');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `first_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `user_group_id`, `first_name`, `last_name`, `user_name`, `email`, `password`, `address`, `phone`, `info`) VALUES
(1, 1, 'Janko', 'Jankovic', 'janko1', 'janko@mail.com', '1122', NULL, NULL, NULL),
(2, 2, 'Marjan ', 'Marjanovic', 'marjan', 'marjan@mail.com', '112233', 'Glavna 23', '011234555', NULL),
(3, 5, 'Ivan', 'Ivanovic', 'ivan', 'ivan@mail.com', '112233', NULL, NULL, NULL),
(4, 3, 'Stefan', 'Stepanovic', 'stefan', 'stefan@mail.com', '112233', 'Adresa 1', '062/1234567', NULL),
(5, 4, 'Srđan', 'Antić', 'srdjan', 'srki@mail.com', '112233', 'Adresa Srki Antic', '061/1234567', NULL),
(6, 3, 'Petar', 'Petrovic', 'petar', 'petar@mail.com', '112233', 'Adresa Petar Petrovic', '064/1234567', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_order`
--

CREATE TABLE `users_order` (
  `id` int(11) NOT NULL,
  `order_code` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `food_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'top-manager', 'Top Menadžer'),
(3, 'Manager', 'Menadžer'),
(4, 'chef', 'Šef kuhinje'),
(5, 'user', 'Korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `quantity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`id`, `ingredient_id`, `quantity`, `amount`, `branch_id`) VALUES
(1, 1, '20kg', 2, 1),
(2, 2, '15kg', 2, 1),
(3, 3, '30l', 0, 1),
(4, 4, '10l', 0, 1),
(5, 5, '23kg', 0, 1),
(6, 6, '14kg', 0, 1),
(7, 7, '90kg', 0, 1),
(8, 8, '5kg', 0, 1),
(9, 9, '900gr', 0, 1),
(10, 10, '6kg', 0, 1),
(11, 11, '8kg', 0, 1),
(12, 12, '4kg', 0, 1),
(13, 13, '3kg', 0, 1),
(14, 14, '11kg', 0, 1),
(15, 15, '20l', 0, 1),
(16, 16, '', 115, 1),
(17, 17, '2.5kg', 0, 1),
(18, 1, '8kg', 0, 2),
(19, 2, '15kg', 0, 2),
(20, 3, '13l', 0, 2),
(21, 4, '9l', 0, 2),
(22, 5, '18kg', 0, 2),
(23, 6, '14kg', 0, 2),
(24, 7, '120kg', 0, 2),
(25, 8, '3.2kg', 0, 2),
(26, 9, '850gr', 0, 2),
(27, 10, '7kg', 0, 2),
(28, 11, '12kg', 0, 2),
(29, 12, '6kg', 0, 2),
(30, 13, '4kg', 0, 2),
(31, 14, '6kg', 0, 2),
(32, 15, '15l', 0, 2),
(33, 16, '', 65, 2),
(34, 17, '5kg', 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_material`
--
ALTER TABLE `food_material`
  ADD KEY `food_id` (`food_id`),
  ADD KEY `material_id` (`material_id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD KEY `user_group_id` (`user_group_id`);

--
-- Indexes for table `users_order`
--
ALTER TABLE `users_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `food_id` (`food_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `ingredient_id` (`ingredient_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users_order`
--
ALTER TABLE `users_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`manager_id`) REFERENCES `users` (`userID`);

--
-- Constraints for table `food_material`
--
ALTER TABLE `food_material`
  ADD CONSTRAINT `food_material_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`),
  ADD CONSTRAINT `food_material_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`);

--
-- Constraints for table `users_order`
--
ALTER TABLE `users_order`
  ADD CONSTRAINT `users_order_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`id`),
  ADD CONSTRAINT `users_order_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`userID`);

--
-- Constraints for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  ADD CONSTRAINT `warehouse_ibfk_2` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`);


