/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import db.DB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DALi
 */
public class Ingredient {
    private int id;
    private String name;

    public Ingredient() {
    }

    public Ingredient(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Ingredient> all() {

        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
        try {
            DB.query("SELECT * FROM ingredients");
            while (DB.result.next()) {
                Integer id = DB.result.getInt("id");
                String name = DB.result.getString("name");
                Ingredient i = new Ingredient(
                        id,
                        name
                );
                ingredients.add(i);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        return ingredients;
    }
    
    public static ArrayList<Ingredient> getIngredients(int branch_id) {

        ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
        try {
            DB.query("SELECT * FROM `ingredients` WHERE id NOT IN (select w.ingredient_id from warehouses w where w.branch_id = "+branch_id+")");
            while (DB.result.next()) {
                Integer id = DB.result.getInt("id");
                String name = DB.result.getString("name");
                Ingredient i = new Ingredient(
                        id,
                        name
                );
                ingredients.add(i);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        return ingredients;
    }
    
}
