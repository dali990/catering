/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import db.DB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DALi
 */
public class Warehouse extends Model{
    private int id;
    private int ingredient_id;
    private String ingredient_name;
    private int branch_id;
    private String branch_name;
    private String quantity;
    private String amount;

    public Warehouse() {
    }

    public Warehouse(int id, int ingredient_id, String ingredient_name, int branch_id, String branch_name, String quantity, String amount) {
        this.id = id;
        this.ingredient_id = ingredient_id;
        this.ingredient_name = ingredient_name;
        this.branch_id = branch_id;
        this.branch_name = branch_name;
        this.quantity = quantity;
        this.amount = amount;
    }
    
    public Warehouse(int ingredient_id, int branch_id, String quantity, String amount) {
        this.ingredient_id = ingredient_id;
        this.branch_id = branch_id;
        this.quantity = quantity;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIngredient_id() {
        return ingredient_id;
    }

    public void setIngredient_id(int ingredient_id) {
        this.ingredient_id = ingredient_id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    public int getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return (amount == "0" || amount == "") ? "":amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    public static ArrayList<Map<String, Object>> getByBranchID(int branchID){
        Branch b = Branch.find(branchID);
        ArrayList<Map<String, Object>> branch_warehouses = new ArrayList<>();
//        Map<Integer, ArrayList<Warehouse>> branch_warehouses = new HashMap<>();
                   
            ArrayList<Warehouse> warehouse_items = new ArrayList<Warehouse>();
            Map<String, Object> map = new HashMap<>();
            try {
                String sql
                        = "SELECT ingredients.name AS ingredient_name, branches.name AS branch_name, amount, quantity, warehouses.id as warehouse_id, "
                        + "ingredients.id AS ingredient_id, branches.id AS branch_id "
                        + "FROM warehouses "
                        + "INNER JOIN ingredients ON warehouses.ingredient_id = ingredients.id "
                        + "INNER JOIN branches ON warehouses.branch_id = branches.id "
                        + "WHERE branches.id = " + branchID +" "
                        + "ORDER BY branches.id, ingredients.name";
                DB.query(sql);
                while (DB.result.next()) {
                    Integer id = DB.result.getInt("warehouse_id");
                    int ingredient_id = DB.result.getInt("ingredient_id");
                    String ingredient_name = DB.result.getString("ingredient_name");
                    int branch_id = DB.result.getInt("branch_id");
                    String branch_name = DB.result.getString("branch_name");
                    String quantity = DB.result.getString("quantity");
                    String amount = DB.result.getString("amount");
                    Warehouse w = new Warehouse(
                            id,
                            ingredient_id,
                            ingredient_name,
                            branch_id,
                            branch_name,
                            quantity,
                            amount
                    );
                    warehouse_items.add(w);
                }
                map.put("branch_name", b.getName());
                map.put("branch_id", b.getId());
                map.put("data", warehouse_items);
                branch_warehouses.add(map);
            } catch (SQLException ex) {
                Logger.getLogger(Warehouse.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DB.close();
            }
        return branch_warehouses;
    }
    
    public static ArrayList<Map<String, Object>> all(){
        ArrayList<Branch> branches = Branch.all();
        ArrayList<Map<String, Object>> branch_warehouses = new ArrayList<>();
//        Map<Integer, ArrayList<Warehouse>> branch_warehouses = new HashMap<>();
        for(Branch b : branches){
                   
            ArrayList<Warehouse> warehouse_items = new ArrayList<Warehouse>();
            Map<String, Object> map = new HashMap<>();
            try {
                String sql
                        = "SELECT ingredients.name AS ingredient_name, branches.name AS branch_name, amount, quantity, warehouses.id as warehouse_id, "
                        + "ingredients.id AS ingredient_id, branches.id AS branch_id "
                        + "FROM warehouses "
                        + "INNER JOIN ingredients ON warehouses.ingredient_id = ingredients.id "
                        + "INNER JOIN branches ON warehouses.branch_id = branches.id "
                        + "WHERE branches.id = " + b.getId().toString() +" "
                        + "ORDER BY branches.id, ingredients.name";
                DB.query(sql);
                while (DB.result.next()) {
                    Integer id = DB.result.getInt("warehouse_id");
                    int ingredient_id = DB.result.getInt("ingredient_id");
                    String ingredient_name = DB.result.getString("ingredient_name");
                    int branch_id = DB.result.getInt("branch_id");
                    String branch_name = DB.result.getString("branch_name");
                    String quantity = DB.result.getString("quantity");
                    String amount = DB.result.getString("amount");
                    Warehouse w = new Warehouse(
                            id,
                            ingredient_id,
                            ingredient_name,
                            branch_id,
                            branch_name,
                            quantity,
                            amount
                    );
                    warehouse_items.add(w);
                }
                map.put("branch_name", b.getName());
                map.put("branch_id", b.getId());
                map.put("data", warehouse_items);
                branch_warehouses.add(map);
            } catch (SQLException ex) {
                Logger.getLogger(Warehouse.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DB.close();
            }
        }
        return branch_warehouses;
    }
    
    public static int insert(Warehouse w) {       
        try {         
            String sql = "INSERT INTO warehouses (ingredient_id, branch_id, quantity, amount) VALUES(?,?,?,?)";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setInt(1, w.getIngredient_id());
            DB.preparedStatement.setInt(2, w.getBranch_id());
            DB.preparedStatement.setString(3, w.getQuantity());
            DB.preparedStatement.setString(4, w.getAmount());

            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
