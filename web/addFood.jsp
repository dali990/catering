<%-- 
    Document   : addAlbum
    Created on : Jan 18, 2018, 2:03:30 AM
    Author     : DALi
--%>

<%@page import="beans.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<%@ include file="layouts/header.jsp" %>
       
        <%@ include file="layouts/menu.jsp" %>

        <div class="content">
            <div class="container-fluid">
                <%@ include file="layouts/msg.jsp" %>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-light">
                                Add new food
                            </div>
                            
                            <form action="AddFood" method="POST" id="addAlbum">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="normal-input" class="form-control-label">Food Name:</label>
                                                <input id="normal-input" class="form-control" name="album_name" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="read-only" class="form-control-label">Artist:</label>
                                                <input id="read-only" class="form-control" name="artist" value="" >
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="disabled-input" class="form-control-label">Duration:</label>
                                                <input id="disabled-input" class="form-control" name="duration" type="number" min="1" style="width:60px" >
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary px-5">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/popper.js/popper.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="./vendor/chart.js/chart.min.js"></script>
<script src="./js/carbon.js"></script>
<script src="./js/demo.js"></script>
<script src="./js/validate.js"></script>
<script>
    (function(){
        let V = new Validate("addAlbum");
        V.setRules({
            album_name:{
                name: "Album name",
                rule: "required|string|max:64"
            },
            artist:{
                name: "Artist",
                rule: "required|string|max:64|min:2"
            },
            duration:{
                name: "Duration",
                rule: "required|number|min_num:1"
            }
        });
        V.run();
    })();
</script>
</body>
</html>

