// Tiny Ajax library
//export var AJAX = {
 var AJAX = {
  createXHR: function ()
  {
    var xhr;
    if (window.ActiveXObject)
    {
      try
      {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert(e.message);
        xhr = null;
      }
    }
    else
    {
      xhr = new XMLHttpRequest();
    }

    return xhr;
  },

  convertToPost: function (data) {
    let postData='';
    for(key in data){
      postData += key+"="+data[key]+"&";
    }
    return postData.slice(0, -1);
  },

  //xhr: this.createXHR,
  XHR: function(){
    let xhr = AJAX.createXHR();
    return xhr;
  },
  //Shows the percent progress while uploading files
  progress: function (param) {
    return new Promise(function(resolve, reject){
      let xhr = AJAX.createXHR();
      xhr.open("POST", param.url, true)
      // xhr default header accept: */* for multi upload
      //xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

      xhr.upload.addEventListener("progress", function(evt){
        if (evt.lengthComputable) {
          var percentComplete = evt.loaded / evt.total;
          //Do something with upload progress
          let _p = Math.round(percentComplete*100);
          resolve(_p);         
        }
      }, false);
      xhr.onerror = function(){
        reject(xhr.status);
        console.log("Ajax error = "+xhr.status);
      }
      if (param.data) {xhr.send(param.data)} else {xhr.send()}
    });
  },
  // Ajax with post method
  post: function (param) {
    let form_data = null;
    if (param.data) {
      form_data = AJAX.convertToPost(param.data);
    } 
    if (param.csrf) {
      form_data += "&"+param.csrf+"="+document.getElementsByName(param.csrf)[0].value;
    }

    return new Promise(function(resolve, reject){
      let xhr = AJAX.createXHR();
      AJAX.XHR = xhr;
      xhr.open("POST", param.url, true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onload = function(){
        if (xhr.status == 200) {
          if (param.responseType == 'text') {
            resolve(xhr.responseText);
          }
          let data = JSON.parse(xhr.responseText);
          resolve(data);
          //Callback
          //param.success(data);
        }
      };
      xhr.onerror = function(){
        reject(xhr.status);
        console.log("Ajax error = "+xhr.status);
      }
      xhr.send(form_data);
    });
  }
  
  /*xhr.onreadystatechange = function()
  {
    if (xhr.readyState === 4)
    {
      alert(xhr.responseText);
    }
  }*/

}


