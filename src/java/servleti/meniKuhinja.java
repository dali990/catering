/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servleti;

import beans.Food;
import db.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DALi
 */
public class meniKuhinja extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet meniKuhinja</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet meniKuhinja at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        int kategorija = Integer.parseInt(request.getParameter("kategorija"));
        ArrayList<Food> foods = new ArrayList<Food>();
        
        
        try {
            DB.query("SELECT * FROM food WHERE category = "+kategorija);
           // if (DB.result.isBeforeFirst()) {
                while(DB.result.next()){
                    String food_name = DB.result.getString("food_name");
                    int category = DB.result.getInt("category");
                    String size = DB.result.getString("size");
                    String description = DB.result.getString("description");
                    String recipe = DB.result.getString("recipe");
                    int price = DB.result.getInt("price");
                    String info = DB.result.getString("info");
                    Food f = new Food(
                            food_name,
                            category,
                            size,
                            description,
                            recipe,
                            price,
                            info
                    );
                    foods.add(f);
                }
            //}
        } catch (SQLException ex) {
            Logger.getLogger(Serach.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        
        request.setAttribute("Foods", foods); 
        request.getRequestDispatcher("meniSlanaKuhinja.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
