<%-- 
    Document   : jspJSTL
    Created on : Jan 20, 2018, 2:32:53 AM
    Author     : DALi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${result != null}">
            <c:forEach items="${result}" var="item">
                ${item}<br>
            </c:forEach>
        </c:if>

        ${user!=null?"Loged in":"Nista"}

        <c:choose>
            <c:when test="${user!=null}">
                <h3>Welcome ${user}</h3>
            </c:when>    
            <c:otherwise>
                <h3>Not Logged in.</h3>
            </c:otherwise>
        </c:choose>
    </body>
</html>
