var Validate = function (id='', rules='') {
	this.debug = false;
	this.rules = rules;
	this.msgElementHTML;
	this.error_obj=[];
	this.err_elem;
	this.input;
	this.runFunction=null;
	this.funparam = null;
	this.styles = {
		error: {
			inputs: "error_input",
			msg: 		"text-danger"		
		},
		success:{
			inputs: "success_input",
			msg: 		"text-success"	
		},
		custom: false
	};
	var self = this;
	//Constructor
	this.ctor = function (id,rules) {
		if (id != '') {
			this.form = document.getElementById(id);
			this.submit = this.form.querySelector("[type='submit']");
		}

		this.attachDefaultCSS();
		console.log(rules)
	};

	this.execute = function(obj,param){
		this.runFunction = obj;
		this.funparam = param;
	};
	
	this.setRules = function (rules) {
		this.rules = rules;
		//this.BeforeSubmit();
	};

	this.css = function (css) {
		this.styles = css;
	};

	this.testrun = ()=>{
		this.form.onsubmit = function (e) {
			e.preventDefault();
			console.log(this.styles);
			console.log(this.runFunction);
			if (this.runFunction != null) {this.runFunction(this.funparam)}
		}
	}
	this.stop = false;

	this.run = function (debug=false) {
		let passed = false;
		if (!this.stop)
		this.form.onsubmit = async function (e) {

			self.error_obj = {};
			//e.preventDefault();
			for (let field_name in self.rules) {
				if(self.selectElements(field_name.toString(),e) == "Array"){
					self.arrayValidation(field_name,e);
				} else {				
					let validation = self.validating(self.input.value, self.rules[field_name]);
					//If validation fails
					if (validation.error || debug) {
						console.log("Nije validan "+self.rules[field_name].name+ ". Greska: "+validation.error.msg);
						passed = false;
						e.preventDefault();
						//If the error div is already inserted (from previos errors)
						if (self.err_elem) {
							self.err_elem.className = self.styles.error.msg;
							self.err_elem.innerHTML = validation.error.msg;
							self.input.classList.remove(self.styles.success.inputs);
							self.input.classList.add(self.styles.error.inputs);
						} else { //For the first error no need for removing succes classes
							let parentNode = self.input.parentNode;
							let msg = document.createElement("div");
							msg.id = "err_msg_"+field_name.toString();
							msg.className = self.styles.error.msg;
							//msg.innerHTML = "Nije validan "+self.rules[field_name].name+ ". Greska: "+validation.error.msg;
                            msg.innerHTML = validation.error.msg;
							parentNode.insertBefore(msg, self.input);
							//If provided custom error class
							if (self.rules[field_name].error_class) {
								self.input.classList.add(self.rules[field_name].error_class);
							} else {
								self.input.classList.add(self.styles.error.inputs);
							}						
						}
					//Validation success
					} else {
						passed = true;
						if (self.err_elem) {
							self.err_elem.innerHTML="";
							self.err_elem.className="";
							self.input.classList.remove(self.styles.error.inputs);
							self.input.classList.add(self.styles.success.inputs);
						}
					}
				}
			}
			// -1 za button submit
			/*for (var i = 0; i < e.srcElement.length-1; i++) {
				let name = e.srcElement[i].name;
				let validation = self.validating(e.srcElement[i].value, self.rules[name]);
				if (validation.error) {
					console.log("Nije validan "+name+ ". Greska: "+validation.error.msg);
				}
			
			}*/
			if(Object.keys(self.error_obj).length == 0){
				e.preventDefault();
			  await self.passed();
			  this.submit();
			}
		}
		//if (passed) this.form.submit();
	};

	this.selectElements = function (field_name,e) {
		this.err_elem = e.target.querySelector('#err_msg_'+field_name);
		this.input = e.target.querySelector('[name="'+field_name+'"]');
		let type = null;
		
		if (self.rules[field_name].array) {
			//this.input = e.target.querySelectorAll('[name="'+field_name+'[]"]');
			this.input = e.target.querySelectorAll('.'+self.rules[field_name].array);
			type = "Array";
		} 
		// Debug
		if (!this.input || this.input == null || this.input == 'undefined') {
			e.preventDefault();
			console.log("Cant finde field "+field_name);
		}
		return type;
	}

	//Custom handler for validation
	this.validation = function (callback) {
		for (field_name in self.rules) {
			let input = this.form.querySelector("[name='"+field_name+"']");
			let validation = self.validating(input.value, self.rules[field_name]);
			//Debug
			if (!input || input == null || input == 'undefined') {
					console.log("Cant finde field "+field_name);
				}
			//If the validation rule has custom error class,then apply to that input
			if (self.rules[field_name].error_class) {
				input.classList.add(self.rules[field_name].error_class);
			} else input.classList.add(this.styles.error.inputs);

			callback(validation);
			validation.error ? "" : this.resetMsgElement(field_name);
		}
	};

	this.ButtonClick = function () {
		this.submit.onclick = function (e) {
			console.log("Submited");
			console.log(e);
		}
	};

	//Validation proccess for each rule
	this.validating = function (input, v_rule) {
		let status = {field:v_rule.name, error:false, type:null};
		let constraints = v_rule.rule.split("|");
		let constrainParam = {};
		let Input = input;

		/*if (input.constructor == NodeList) {
			Input = input;
		} else Input = input.value;*/

		for (var i = 0; i < constraints.length; i++) {	
			//Adding rules with parameters if exists
			if (constraints[i].search(":") != -1 ) {
				let customCons = constraints[i].split(":");
				//constrainParam[customCons[0]] = customCons[1]; // {max: 64}
				constraints[i] = customCons[0]; //(constraints[i] = max)
				Input = {value: input, param: customCons[1]}
			} 

			if (v.hasOwnProperty(constraints[i])) {
				let result = v[constraints[i]](Input, {name:v_rule.name});
				if (result) {
					if (v_rule.hasOwnProperty('msg') && v_rule.msg.hasOwnProperty(constraints[i])) {
						status.error = {
							msg: v_rule.msg[constraints[i]]
						}
					} else {
						status.error = {
							msg: result
						}
					}			  	
					this.error_obj[v_rule.name] = status;
					return status;
				} 
			}
		}
		return status;
		//console.log("Validating "+input+" - "+rule);
	};

	// Validating inputs array
	this.arrayValidation = function (field_name, e) {
		//console.log(this.input);
		for (var i = this.input.length - 1; i >= 0; i--) {		
			let validation = this.validating(this.input[i].value, this.rules[field_name]);
			this.handleArrayValidation(validation, field_name, e, i);
		}
		// Validacija sa odredjenim array kljucevima
		/*for(let key in this.input) {
			let validation = this.validating( this.input[key].value, this.rules[field_name]);
			this.handleArrayValidation(validation, field_name, e, key);
		}*/
	}

	// Handles the message after validation
	this.handleArrayValidation = function (validation, field_name, e, i) {
		let passed = false;
		let err_elem_array = self.input[i].parentNode.querySelector("#err_msg_"+field_name.toString()+"_"+i);
		
		if (validation.error || this.debug) {
			console.log("Nije validan "+self.rules[field_name].name+ ". Greska: "+validation.error.msg);
			passed = false;
			e.preventDefault();
			//If the error div is already inserted (from previos errors)
			if (err_elem_array) {
				err_elem_array.className = self.styles.error.msg;
				err_elem_array.innerHTML = validation.error.msg;
				self.input[i].classList.remove(self.styles.success.inputs);
				self.input[i].classList.add(self.styles.error.inputs);
			} else { //For the first error no need for removing succes classes
				let parentNode = self.input[i].parentNode;
				let msg = document.createElement("div");
				msg.id = "err_msg_"+field_name.toString()+"_"+i;
				msg.className = self.styles.error.msg;
				//msg.innerHTML = "Nije validan "+self.rules[field_name].name+ ". Greska: "+validation.error.msg;
				msg.innerHTML = validation.error.msg;
				parentNode.insertBefore(msg, self.input[i]);
				//If provided custom error class
				if (self.rules[field_name].error_class) {
					self.input[i].classList.add(self.rules[field_name].error_class);
				} else {
					self.input[i].classList.add(self.styles.error.inputs);
				}						
			}
		//Validation success
		} else {
			passed = true;
			if (err_elem_array) {
				self.input[i].parentNode.removeChild(err_elem_array);
				self.input[i].classList.remove(self.styles.error.inputs);
				self.input[i].classList.add(self.styles.success.inputs);
			}
		}
	}

	//Validating custom data
	this.check = function (param) {
		if(param.constructor === Array){
			let status = [];
			for (var i = param.length - 1; i >= 0; i--) {
				let validation = this.validating(param.value, param);
				//If validation fails
				if (validation.error) {
					status[param.name] = {
						error: {
							msg: validation.error.msg,
							vmsg: "Error on "+param.name+" field. "+validation.error.msg
						},
						name: param.name
					}
				}
			}
			return status;
		}
		let validation = this.validating(param.value, param);
		if (validation.error) {
			let status = {
				error: {
					msg: validation.error.msg,
					vmsg: "Error on "+param.name+" field. "+validation.error.msg
				},
				name: param.name
			}
			return status;
		} 
		return true;
	}

	//Register custom message element
	this.msgElement = function (id) {
		this.msgElementHTML = this.form.querySelector("#"+id);
		return this.msgElementHTML;
	}

	//Resets the message elements
	this.resetMsgElement = function (input_name="") {
		if(this.msgElementHTML){
			this.msgElementHTML.innerHTML="";
		}
		if (input_name) {
			this.form[input_name].classList.remove(this.rules[input_name].error_class);
		}
	},

	this.attachDefaultCSS = function () {
		let code = `
			.error_input{
			    border: 1px solid red;
			}
			.text-danger{color:red;}
			.tex-success{color:lightgreen;}
			.success_input{
			    border: 1px solid green;
			}
		`;
		let style = document.createElement('style');
    style.type = 'text/css';

    if (style.styleSheet) {
        // IE
        style.styleSheet.cssText = code;
    } else {
        // Other browsers
        style.innerHTML = code;
    }

    document.head.appendChild( style );
	}

	//Start proces if the validation passes
	this.passed =  ()=>{
		if (self.runFunction != null) {
			return self.runFunction(self.funparam);
		};
	}

	this.ctor(id,rules);
}

const v = {
	required: function(input,field){
		//console.log("R "+input);
		if (input == "") {
			//return "Empty field | required"; 		
			return V_LNG.RS.required.r(field.name);
		} return null;
	},
	string: function(input, field){
		//console.log("S "+input);
		if (typeof input != "string") {
			return "Not a string";
		} 
		if (/^[<>]+$/.test(input)) {
			return "Forbidden characters."
		}
		  return null;
	},
	number: function(input, field){
		//console.log("N "+input);
		if (isNaN(input)) {
			//return "Not a number";
			return V_LNG.RS.number.r(field.name);
		} return null;
	},
	number_value: function(input, field){ //Za dropdown polja
		if (isNaN(input)) {
			return V_LNG.RS.number_value.r(field.name);
		} return null;
	},
    email: function(email, field) {
      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!re.test(email.toLowerCase())){
          //return "Invalid email";
          return V_LNG.RS.email.r(field.name);
      } return null;
    },
	max: function(input,field){
		/*console.log("MAX ");
		console.log(input);*/
		if (input.value.length > parseInt(input.param)) {
			//return "To many characters, max: "+input.param;
			return V_LNG.RS.max.r(field.name, input.param);
		} return null;
	},
	min: function(input, field){
		if (parseInt(input.value.length) < parseInt(input.param)) {
			return "Min " +input.param+" characters."
		} return null;
	},
	max_num: function(input, field){
		/*console.log("max_num ");
		console.log(input);*/
		if (parseInt(input.value) > parseInt(input.param)) {
			return "To big number, max: "+input.param;
		} return null;
	},
	min_num: function(input, field){
		/*console.log("max_num ");
		console.log(input);*/
		if (parseInt(input.value) < parseInt(input.param)) {
			return "Number must be greater than: "+(parseInt(input.param)-1);
		} return null;
	},
	array_check: function (input, field) {
		return v[input.param](input.value);
		console.log(input);
	}
}

const V_LNG = {
	RS : {
		required: {
			msg:"Polje {field} nesme biti prazno.",
			r(field_name){
				return this.msg.replace("{field}",field_name);
			}
		},
		number  : {
			msg: "Polje {field} moze da sadrzi samo brojeve.",
			r(field_name){
				return this.msg.replace("{field}",field_name);
			}
		},
		max     : {
			msg: "Polje {field} može da sadrži najviše {param} karaktera.",
			r(field_name, param){
				return this.msg.replace("{field}",field_name).replace("{param}",param);
			}
		},
		min     : {
			msg: "Polje {field} ne može da sadrži više od {param} karaktera.",
			r(field_name, param){
				return this.msg.replace("{field}",field_name).replace("{param}",param);
			}
		},
		email   : {
			msg: "E-mail adresa nije validna",
			r(field_name){
				return this.msg.replace("{field}",field_name);
			}
		},
	},
	EN : {
		required: "Field can not be empty.",
		number  : "Field can contain only numbers."
	}
}