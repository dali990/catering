new Vue({
    el: "#vue-app",
    data () {
        return {
          albums: [],
          search: ''
        };
    },
    
    methods: {

    },
    created(){
        AJAX.post({url:"AjaxCall"}).then(d=>{console.log(d)});
    },
    // Pokrece metode samo za one varijale koje su promenile vrednost
    computed: {
    filterAlbums: function () {
      return this.albums.filter((album) => {
        return album.name.match(this.search);
      });
    }
    
    }
});



