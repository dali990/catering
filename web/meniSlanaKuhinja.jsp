<%-- 
    Document   : meniSlanaKuhinja
    Created on : Feb 19, 2018, 9:46:41 PM
    Author     : DALi
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="beans.Food"%>
<%@ include file="layouts/header.jsp" %>
<%@ include file="layouts/menu.jsp" %>

    <%  
        ArrayList<Food> foods = (ArrayList<Food>) request.getAttribute("Foods"); 
    %> 

<div class="content" id="vue-app">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        Slana kuhinja
                    </div>
                    <div class="card-body">
                      
                    </div> 
                </div>                  
            </div>
        </div>
        
        <% if(foods != null ){ %>  
            
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-light">
                        Hoverable Table results
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Naziv</th>
                                    <th>Opis</th>
                                    <th>Cena</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% for(Food f : foods){ %>
                                <tr>
                                    <td class="text-nowrap"><%=f.getFood_name()%></td>
                                    <td><%=f.getDescription()%></td>
                                    <td><%=f.getPrice()%></td>
                                </tr>
                                <% } %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% } else { %>
            No results
        <% } %>
    </div>
        
</div>
    

</body>
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/popper.js/popper.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="./vendor/chart.js/chart.min.js"></script>
<script src="./js/carbon.js"></script>
<script src="./js/demo.js"></script>
<script src="./js/validate.js"></script>

</html>