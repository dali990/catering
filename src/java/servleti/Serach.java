/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servleti;

import beans.Album;
import db.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DALi
 */
public class Serach extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Serach</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Serach at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        PrintWriter out = response.getWriter();
                
        ArrayList<Album> albums = new ArrayList<Album>();
        String sort = request.getParameter("sort");
        String album_name = request.getParameter("album_name");
        String artist = request.getParameter("artist");
        int duration;
        try {
            duration = Integer.parseInt(request.getParameter("duration"));
        } catch(NumberFormatException e){
            duration = 0;
        } 
        
        //albums.add(new Album(album_name,artist,duration));
        
        DB.searchAlbum(album_name, artist, duration, sort);
//        DB.searchByArtist(artist);
//        out.println(DB.last_query);

//         String url = "jdbc:mysql://localhost:3306/melodyhouse";
//         String user = "root";
//         String pass = "";
//         
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            Connection conn = DriverManager.getConnection(url, user, pass);
//            Statement stmt = conn.createStatement();
//            String query = "SELECT * FROM albums ";
//       
//            ResultSet result = stmt.executeQuery(query);
//            
//            while(result.next()){
//               String an = result.getString("album_name");
//                String ar = result.getString("artist");
//                int d = result.getInt("duration");
//                Album a = new Album(an,ar,d);
//                albums.add(a); 
//            }
//            conn.close();
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(Serach.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SQLException ex) {
//            Logger.getLogger(Serach.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            
//        }
        
        try {
            while(DB.result.next()){
                String an = DB.result.getString("album_name");
                String ar = DB.result.getString("artist");
                int d = DB.result.getInt("duration");
                Album a = new Album(an,ar,d);
                albums.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Serach.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
//        for (Album a : albums) {
//            out.println(a.getAlbum_name());
//            out.println(a.getArtist());
//            out.println("-------------------------------\n");
//        }
        request.setAttribute("Albums", albums); 
        request.getRequestDispatcher("normalSearch.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
