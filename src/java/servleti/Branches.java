/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servleti;

import beans.Branch;
import beans.User;
import db.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DALi
 */
public class Branches extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        /*------------------------------------------------------------------------
        UPDATE
        */
        if (request.getParameterMap().containsKey("update")) 
        {
            int id = Integer.parseInt(request.getParameter("update"));
            update(request, response, id);
        } 
        /*------------------------------------------------------------------------
        INSERT
        */
        else if (request.getParameterMap().containsKey("insert")) 
        {
            insert(request, response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        
        /*----------------------------------------------------------------  
        EDIT
        */
        if (request.getParameterMap().containsKey("edit")) 
        {
            int edit_id = Integer.parseInt(request.getParameter("edit"));
            editBranch(request, response, edit_id);
            
        } 
        /*----------------------------------------------------------------------------------
        DELETE
        */
        else if (request.getParameterMap().containsKey("delete"))
        {          
            int id = Integer.parseInt(request.getParameter("delete"));
            int deleted = Branch.delete(id);
            if (deleted > 0) {
                session.setAttribute("msg_ok", "Uspesno izrisano!"); 
                response.sendRedirect("Branches");
            } else {   
                session.setAttribute("msg_error", "Greska tokom brisanja!"); 
                response.sendRedirect("Branches?edit="+id);
            }
        }
        /*--------------------------------------------------------------------------------------
        INSERT
        */
        else if (request.getParameterMap().containsKey("addNewBranch")) 
        {
            addNewBranch(request, response);
        }
        /*--------------------------------------------------------------------------------------
        SHOW
        */
        else
        {
            ArrayList<Branch> branches = Branch.all();

            request.setAttribute("branches", branches); 
            request.getRequestDispatcher("branches/branches.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void editBranch(HttpServletRequest request, HttpServletResponse response, Integer edit_id) throws ServletException, IOException{
        Branch branch = null;
        ArrayList<User> users = new ArrayList<User>();
        try {
            DB.query("SELECT * FROM branches INNER JOIN users ON branches.manager_id = users.userID WHERE branches.id = "+edit_id);
            while(DB.result.next()){
                Integer id = DB.result.getInt("id");
                String name = DB.result.getString("name");
                String address = DB.result.getString("address");
                String phone = DB.result.getString("phone");
                String manager = DB.result.getString("users.first_name")+" "+DB.result.getString("users.last_name");
                Integer manager_id = DB.result.getInt("manager_id");
                branch = new Branch(
                    id,
                    name,
                    address,
                    phone,
                    manager_id,
                    manager
                );
            }

        } catch (SQLException ex) {
            Logger.getLogger(Branches.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }

        try {
            DB.query("SELECT * FROM users WHERE user_group_id = 2 OR user_group_id=3");
            while(DB.result.next()){
                Integer id = DB.result.getInt("userID");
                String first_name = DB.result.getString("first_name");
                String last_name = DB.result.getString("last_name");
                String user_name = DB.result.getString("user_name");
                Integer user_group_id = DB.result.getInt("user_group_id");
                String email = DB.result.getString("email");
                String address = DB.result.getString("address");
                String phone = DB.result.getString("phone");

                User user = new User(
                    id,
                    first_name,
                    last_name,
                    user_name,
                    user_group_id,
                    email,
                    address,
                    phone
                );
                users.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Branches.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }

        request.setAttribute("branch", branch); 
        request.setAttribute("users", users); 
        request.getRequestDispatcher("branches/editBranch.jsp").forward(request, response);
    }
    
    private void update(HttpServletRequest request, HttpServletResponse response, Integer id) throws IOException, ServletException{
        
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        Integer manager_id = Integer.parseInt(request.getParameter("manager_id"));
        
        Branch b = new Branch(id, name, address, phone, manager_id);
        Integer saved = Branch.update(b);
        
        HttpSession session = request.getSession();
        if (saved>0) {
            session.setAttribute("msg_ok", "Uspesno sacuvano!"); 
            response.sendRedirect("Branches");
        } else {   
            session.setAttribute("msg_error", "Greska tokom cuvanja!"); 
            response.sendRedirect("Branches?edit="+id);
        }
        //
    }
    
    private void addNewBranch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<User> users = new ArrayList<User>();
        try {
            DB.query("SELECT * FROM users WHERE user_group_id = 2 OR user_group_id=3");
            while (DB.result.next()) {
                Integer id = DB.result.getInt("userID");
                String first_name = DB.result.getString("first_name");
                String last_name = DB.result.getString("last_name");
                String user_name = DB.result.getString("user_name");
                Integer user_group_id = DB.result.getInt("user_group_id");
                String email = DB.result.getString("email");
                String address = DB.result.getString("address");
                String phone = DB.result.getString("phone");

                User user = new User(
                        id,
                        first_name,
                        last_name,
                        user_name,
                        user_group_id,
                        email,
                        address,
                        phone
                );
                users.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Branches.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        request.setAttribute("users", users); 
        request.getRequestDispatcher("branches/addNewBranch.jsp").forward(request, response);
    }
    
    private void insert(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        Integer manager_id = Integer.parseInt(request.getParameter("manager_id"));
        
        Branch b = new Branch(name, address, phone, manager_id);
        int saved = Branch.insert(b);
        
        HttpSession session = request.getSession();
        if (saved>0) {
            session.setAttribute("msg_ok", "Uspesno sacuvano!"); 
            response.sendRedirect("Branches");
        } else {   
            session.setAttribute("msg_error", "Greska tokom cuvanja!"); 
            response.sendRedirect("Branches?addNewBranch");
        }
        //
    }
}
