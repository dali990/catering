<%-- 
    Document   : msg
    Created on : Feb 20, 2018, 11:58:08 PM
    Author     : DALi
--%>

<%@page import="beans.User"%>
<div class="row">
    <div class="col-md-12">
        <% String poruka = (String)request.getAttribute("poruka"); 
            String error = (String)request.getAttribute("error"); 
            User user = (User)request.getAttribute("user"); 
         if(poruka != null){
         %>
             <div class="alert alert-dismissible alert-success">
                 <%=poruka%>
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
         <% } 

         if(error != null){
         %>
             <div class="alert alert-dismissible alert-danger">
                 <%=error%>
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
         <% } %>
         
         
        <%    
            String msg_ok = (String)session.getAttribute("msg_ok");
            String msg_error = (String)session.getAttribute("msg_error");
        %> 
        
        <% if (msg_ok != null) { %>
            <div class="alert alert-dismissible alert-success">
                ${msg_ok}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
            </div>
            <% session.removeAttribute("msg_ok"); %>
        <% } else if (msg_error != null) { %>
            <div class="alert alert-dismissible alert-danger">
                ${msg_error}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
            </div>
            <% session.removeAttribute("msg_error"); %>
        <% } %>
    </div>
</div>
