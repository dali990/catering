<%-- 
    Document   : warehouses
    Created on : Sep 16, 2018, 10:16:39 PM
    Author     : DALi
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="beans.Warehouse"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>

<%@ include file="../layouts/header.jsp" %>      
<%@ include file="../layouts/menu.jsp" %>
<%    
    ArrayList<Map<String, Object>> branch_warehouses = (ArrayList<Map<String, Object>>) request.getAttribute("branch_warehouses");
%> 
<div class="content">
    <%@ include file="../layouts/msg.jsp" %>
    <div class="container-fluid">

        <% for(Map<String, Object> warehouse_map : branch_warehouses) {
        ArrayList<Warehouse> warehouse_list = (ArrayList<Warehouse>)warehouse_map.get("data"); 
        String branch_name = (String)warehouse_map.get("branch_name");
        Integer branch_id = (Integer)warehouse_map.get("branch_id"); %>
        
        <div class="card">
            <div class="card-header bg-light">
                <%=branch_name%>
            </div>

            <div class="card-body">
                <% // if (warehouse_item.getValue().size() > 0) { %>  
                <% 
                    if (warehouse_list.size()>0) { 
                %>  
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Naziv poslovnice</th>
                                <th>Namirnica</th>
                                <th>Kolicina</th>
                                <th>Komada</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% for (Warehouse w : warehouse_list) {%>
                            <tr>
                                <td class="text-nowrap"><%=w.getBranch_name()%></td>
                                <td><%=w.getIngredient_name()%></td>
                                <td><%=w.getQuantity()%></td>
                                <td><%=w.getAmount()%></td>
                                <td>
                                    <a href="Warehouses?edit=<%=w.getId()%>" class="btn btn-outline-secondary btn-sm">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="Warehouses?delete=<%=w.getId()%>"  class="btn btn-outline-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <% }; %>
                        </tbody>
                    </table>
                </div>
                <% } else { %>
                No results
                <% }%>
                <hr>
                <a href="Warehouses?addNewWarehouseItem=true&branch_id=<%=branch_id%>" class="btn btn-outline-primary btn-sm">
                    Nova namirnica <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <% } %>
    </div>
</div>

<%@ include file="../layouts/footer.jsp" %>