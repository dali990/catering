(function(){
    let V = new Validate("logovanje");
    V.setRules({
        username: {
            name: "Username",
            rule: "required|string|max:64"
        },
        password:{
            name: "Password",
            rule: "required|string|max:64|min:4"
        }
    });
    V.run();
})();


