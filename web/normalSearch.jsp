<%-- 
    Document   : normalSearch
    Created on : Jan 22, 2018, 2:52:38 AM
    Author     : DALi
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="beans.Album"%>
<%@ include file="layouts/header.jsp" %>
<%@ include file="layouts/menu.jsp" %>

    <%  
        ArrayList<Album> albums = (ArrayList<Album>) request.getAttribute("Albums"); 
    %>    

    
    <div class="content" id="vue-app">
        <form action="Serach" method="POST" id="search">
            <div class="row">
                <div class="col-md-2">
                    Sort by:
                    <select name="sort" class="form-control">
                        <option value="album_name">Album Name</option>
                        <option value="artist">Artist Name</option>
                        <option value="duration">Duration</option>
                    </select>
                </div>
                <div class="col-md-3">
                    Album:
                    <input type="text" class="form-control" name="album_name" placeholder="Search by album name...">
                </div>
                <div class="col-md-3">
                    Artist:
                    <input type="text" class="form-control" name="artist" placeholder="Search by artist name...">
                </div>
                <div class="col-md-1">
                    Duration
                    <input type="number" min="1" class="form-control" name="duration" placeholder="min">
                </div>
                <div class="col-md-1">
                    <br>
                    <button type="submit" class="btn btn-primary px-3">Search</button>
                </div>            
            </div>
        </form>
        <br>
        
            <% if(albums != null ){ %>  
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Hoverable Table results
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Album Name</th>
                                        <th>Artist</th>
                                        <th>Duration</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <% for(Album a : albums){ %>
                                    <tr>
                                        <td class="text-nowrap"><%=a.getAlbum_name()%></td>
                                        <td><%=a.getArtist()%></td>
                                        <td><%=a.getDuration()%></td>
                                    </tr>
                                    <% } %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
        </div>

                
</body>
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/popper.js/popper.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="./vendor/chart.js/chart.min.js"></script>
<script src="./js/carbon.js"></script>
<script src="./js/demo.js"></script>
<script src="./js/validate.js"></script>
<script>
    (function(){
        let V = new Validate("search");
        V.setRules({
            sort:{
                name: "Sorting",
                rule: "string|max:64"
            },
            album_name:{
                name: "Album",
                rule: "string|max:64"
            },
            artist:{
                name: "Artist",
                rule: "string|max:64"
            },
            duration:{
                name: "Druration",
                rule: "number|min_num:1"
            }
        });
        V.run();
    })();

</script>
</html>