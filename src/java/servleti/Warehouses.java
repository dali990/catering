/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servleti;

import beans.Branch;
import beans.Ingredient;
import beans.User;
import beans.Warehouse;
import db.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DALi
 */
public class Warehouses extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();
        
        /*------------------------------------------------------------------------
        UPDATE
        */
        if (request.getParameterMap().containsKey("update")) 
        {
            int id = Integer.parseInt(request.getParameter("update"));
//            update(request, response, id);
        } 
        /*------------------------------------------------------------------------
        INSERT
        */
        else if (request.getParameterMap().containsKey("addNewWarehouseItem")) 
        {
            int branch_id = Integer.parseInt(request.getParameter("branch_id"));
            addNewWarehouseItem(request, response, branch_id);
        }
        else if (request.getParameterMap().containsKey("insert")) 
        {
            insert(request, response);
        }
        else
        {
            User u = (User)request.getSession(true).getAttribute("user");
//
////            Map<Integer, ArrayList<Warehouse>> branch_warehouses = Warehouse.all();
            ArrayList<Map<String, Object>> branch_warehouses = null;
            if (u.getUser_group_id() < 3) {
                branch_warehouses = Warehouse.all();
            }
//            else {
//                int branch_id = Branch.getByManagerID(u.getUser_group_id()).getId();
//                branch_warehouses = Warehouse.getByBranchID(branch_id);
//            }
            

            request.setAttribute("branch_warehouses", branch_warehouses);
            request.getRequestDispatcher("warehouses/warehouses.jsp").forward(request, response);
        }      
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void addNewWarehouseItem(HttpServletRequest request, HttpServletResponse response,int branch_id) throws IOException, ServletException{
        ArrayList<Ingredient> ingredients = Ingredient.all();
        
        request.setAttribute("branch_id", branch_id); 
        request.setAttribute("ingredients", ingredients); 
        request.getRequestDispatcher("warehouses/addNewWarehouseItem.jsp").forward(request, response);
    }
    
    private void insert(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        
        int ingredient_id = Integer.parseInt(request.getParameter("ingredient_id"));
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        String quantity = request.getParameter("quantity");
        String amount = request.getParameter("amount");
        
        Warehouse w = new Warehouse(ingredient_id, branch_id, quantity, amount);
        int saved = Warehouse.insert(w);
        
        HttpSession session = request.getSession();
        if (saved>0) {
            session.setAttribute("msg_ok", "Uspesno sacuvano!"); 
            response.sendRedirect("Warehouses");
        } else {   
            session.setAttribute("msg_error", "Greska tokom cuvanja!"); 
            response.sendRedirect("Warehouses?addNewWarehouseItem");
        }
        //
    }
}
