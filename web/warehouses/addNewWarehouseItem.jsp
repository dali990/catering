<%-- 
    Document   : addNewWarehouseItem
    Created on : Sep 18, 2018, 9:11:18 PM
    Author     : DALi
--%>

<%@page import="beans.Ingredient"%>
<%@page import="beans.Branch"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="beans.User"%>
<%@page isELIgnored="false" %>

<%@ include file="../layouts/header.jsp" %>      
<%@ include file="../layouts/menu.jsp" %>

<%    
    Integer branch_id = (Integer)request.getAttribute("branch_id");
    Branch b = Branch.find(branch_id);
    ArrayList<Ingredient> ingredients = Ingredient.getIngredients(branch_id);
%> 

<style>
    .custom-plus{font-size: 20px; font-weight: 600;}
    .food-btn{display: inline-block; padding: 5px;}
    .my-card{display: inline-block !important;}
</style>
<div class="content">
    <%@ include file="../layouts/msg.jsp" %>
    <div class="container-fluid">

        <div class="card">
            <div class="card-header bg-light">
                Unos nove poslovnice
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <% for (Ingredient i : ingredients) { %>
                        <div class="food-btn">
                            <button class="btn btn-rounded btn-primary" onclick="addFood('<%=i.getName()%>','<%=i.getId()%>', '<%=branch_id%>')">
                                <%=i.getName()%> <span class="custom-plus">+</span>
                            </button>
                        </div>
                        <% } %>
                    </div>
                </div>
                    <div class="row" id="form_container">
                          
                    </div>
                </div>               
            </div>
        </div>

    </div>
</div>
                            
                    

<script src="./js/validate.js"></script>
<script>
    
    var form_container = document.getElementById('form_container');
    var savebtn = document.getElementById('savebtn');
    var list = [];
    function addFood(ingredient_name, ingredient_id, branch_id){
        var result = list.filter(function(element) {
            return element == ingredient_id;
          });
          console.log(result);
        if(result.length == 0){
            var field = `
            <div class="col-md-3" id="insert_item`+ingredient_id+`">
            <div class="card ">
                <div class="card-header">`+ingredient_name+`</div>
                <div class="card-body">
                    <form  action="Warehouses?insert" method="POST">
                        <input type="hidden" name="branch_id" value="`+branch_id+`">
                        <input type="hidden" name="ingredient_id" value="`+ingredient_id+`">
                        <div class="form-group">
                            <input name="quantity" class="form-control" placeholder="Kolicina" value="">               
                        </div>
                        <div class="form-group">
                            <input name="amount" class="form-control" placeholder="Komada" value="">
                        </div>
                        <div class="form-group" id="savebtn">
                            <button type="submit" class="btn btn-primaryl">Sacuvaj</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>
            `;
            form_container.innerHTML += field;
            list.push(ingredient_id);
        }
    }
    function hide(ing_id){
        var insert_item = document.getElementById('insert_item_'+ing_id);
        insert_item.style.display = "none";
    }
    (function(){
        let V = new Validate("insertItem");
        V.setRules({
            name:{
                name: "Naziv",
                rule: "required|string|max:64",
                msg: {required:"Polje naziv je obavezno"}
            },
            address:{
                name: "Adresa",
                rule: "required|string|max:64"
            },
            phone:{
                name: "Telefon",
                rule: "required|string|min:9"
            },
            manager_id:{
                name: "Menadzer",
                rule: "required|number|max:3|min:1",
                msg: {number:"Nedozovljena vrednost"}
            }
        });
        V.run();
    })();
</script>

<%@ include file="../layouts/footer.jsp" %>
