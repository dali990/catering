/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servleti;
import beans.User;
import db.DB;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author DALi
 */
public class Logovanje extends HttpServlet {

    public Logovanje() {
        
    }
    
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        ResultSet results;
        HttpSession session;
        User user = new User();
        //ArrayList<String> rl = new ArrayList<String>();
        if (username !=null && password !=null) {
            try {            
                DB.checkUser(username, password);
                while(DB.result.next()){
                    user.setFirst_name(DB.result.getString("first_name"));
                    user.setLast_name(DB.result.getString("last_name"));
                    user.setUser_name(DB.result.getString("user_name"));
                    user.setUser_group_id(DB.result.getInt("user_group_id"));
                    user.setId(DB.result.getInt("userID"));
                    user.setUser_name(DB.result.getString("user_name"));
                    user.setPassword(DB.result.getString("password"));
                    DB.close();
                }               
            } catch (SQLException ex) {
                Logger.getLogger(Logovanje.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            if (username.equals(user.getUser_name())
                && password.equals(user.getPassword())) {
                request.setAttribute("poruka", "Dobrodosli!");
                request.setAttribute("user", user);
                session = request.getSession();
                session.setAttribute("user", user);
                //request.getRequestDispatcher("home.jsp").forward(request, response);
                response.sendRedirect("home.jsp");
            } else {
               request.setAttribute("error", "Neispravni podaci!"); 
               request.getRequestDispatcher("login.jsp").forward(request, response);
               response.sendRedirect("login.jsp");
            }
        //Prazna polja    
        } else { 
            request.setAttribute("error", "Neispravno podaci!"); 
            request.getRequestDispatcher("login.jsp").forward(request, response);
            response.sendRedirect("login.jsp");
        }
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
