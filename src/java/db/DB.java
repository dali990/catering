/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import beans.Food;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DALi
 */
public class DB {
    private static Connection conn = null;
    private static Statement stmt = null;
    public static ResultSet result = null;
    public static PreparedStatement preparedStatement = null;
    public static String last_query = "";
    public static String where = " WHERE ";
    public static int w_count = 0;
    
    static String url = "jdbc:mysql://localhost:3306/catering";
    static String user = "root";
    static String pass = "";
    
    public static Connection getConn()throws SQLException{   
        try{
           Class.forName("com.mysql.jdbc.Driver");
           conn = DriverManager.getConnection(url, user, pass);
           return conn;           
        } catch(Exception e){
                System.out.println(e);
               return null;
        }  
    }
    
    public static Statement getStmt() throws SQLException{
        return DB.getConn().createStatement();
    }
    
    public static ResultSet query(String query) {
        ResultSet result = null;
        try {
            Statement stmt = DB.getStmt();
            result = stmt.executeQuery(query);
            DB.result = result;
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public static void checkUser(String username, String password){  
        
        try {
            DB.preparedStatement = DB.getConn().prepareStatement("SELECT * FROM users WHERE user_name = ? AND password = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            DB.result = preparedStatement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
              
    }
    
    public static void getAlbums(){
        try {
            DB.stmt = DB.getStmt();
            DB.result = DB.stmt.executeQuery("SELECT * FROM albums");         
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static int insertAlbum(String album_name, String artist, int duration){
        
        try {         
            String sql = "INSERT INTO albums (album_name, artist, duration) VALUES(?,?,?)";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setString(1, album_name);
            DB.preparedStatement.setString(2, artist);
            DB.preparedStatement.setInt(3, duration);
            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public static int insertFood(Food food){
        try {         
            String sql = "INSERT INTO food (food_name, category, size, description, directions, price, info) VALUES(?,?,?,?,?,?,?)";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setString(1, food.getFood_name());
            DB.preparedStatement.setInt(2, food.getCategory());
            DB.preparedStatement.setString(3, food.getSize());
            DB.preparedStatement.setString(4, food.getDescription());
            DB.preparedStatement.setString(5, food.getRecipe());
            DB.preparedStatement.setInt(6, food.getPrice());
            DB.preparedStatement.setString(7, food.getInfo());

            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    
    
    public static void searchAlbum(String album_name, String artist, int duration, String sort){
        String query = "SELECT * FROM albums ";
        if (album_name != "" && album_name.length() > 0) {
            DB.Where("album_name LIKE '%"+album_name+"%'");
        }
        if (artist != "" && artist.length() > 0) {
            DB.Where("artist LIKE '%"+artist+"%'");
        }
        if (duration > 0) {
            DB.Where("duration = "+duration);
        }
        query += DB.where;
        if (sort != "" && sort.length() > 0) {
            query += " ORDER BY "+sort;
        }
        
        try {
            DB.stmt = DB.getStmt();
            DB.result = DB.stmt.executeQuery(query);
            DB.last_query = query;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }             
    }
    
    public static void get(String table){
        String query = "SELECT * FROM "+table;
        if (w_count > 0) {
            query += " WHERE category = 1";
        }
        try {
            DB.stmt = DB.getStmt();
            DB.result = DB.stmt.executeQuery(query);
            DB.last_query = query;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void searchByArtist(String artist){
        String query = "SELECT * FROM albums WHERE artist LIKE '%"+artist+"%' ORDER BY artist";
        try {
            DB.stmt = DB.getStmt();
            DB.result = DB.stmt.executeQuery(query);
            DB.last_query = query;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void Where(String w){
        if (w_count > 0) {
            DB.where += " AND "+w;
        } else DB.where += w;
        
        w_count++;
    }
    
     public static void close() {
        try {
            if (result != null) {
                result.close();
                
            }

            if (stmt != null) {
                stmt.close();
            }

            if (conn != null) {
                conn.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (where != " WHERE ") {
                where = " WHERE ";
            }
            if (last_query != "") {
                last_query = "";
            }
        } catch (Exception e) {

        }
    }
}
