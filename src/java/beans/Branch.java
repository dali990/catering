/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import db.DB;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DALi
 */
public class Branch 
{
    protected String table_name = "branches";
    private Integer id;
    private String name;
    private String address;
    private String phone; 
    private Integer manager_id;
    private String manager;

    public Branch(Integer id, String name, String address, String phone, Integer manager_id, String manager) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.manager_id = manager_id;
        this.manager = manager;
    }
    
    public Branch(Integer id, String name, String address, String phone, Integer manager_id) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.manager_id = manager_id;
    }
    
    public Branch(String name, String address, String phone, Integer manager_id) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.manager_id = manager_id;
    }

    private Branch() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getManager_id() {
        return manager_id;
    }

    public void setManager_id(Integer manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }
    
    public static Branch find(int id){
        Branch b = null;
        try {
            DB.query("SELECT * FROM branches INNER JOIN users ON branches.manager_id = users.userID where id = "+id);
            while (DB.result.next()) {
                Integer branch_id = DB.result.getInt("id");
                String name = DB.result.getString("name");
                String address = DB.result.getString("address");
                String phone = DB.result.getString("phone");
                String manager = DB.result.getString("users.first_name") + " " + DB.result.getString("users.last_name");
                Integer manager_id = DB.result.getInt("manager_id");
                b = new Branch(
                        branch_id,
                        name,
                        address,
                        phone,
                        manager_id,
                        manager
                );
//                return b;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        return b;
    }
    
    public static Branch getByManagerID(int id){
        Branch b = null;
        try {
            DB.query("SELECT * FROM branches INNER JOIN users ON branches.manager_id = users.userID where manager_id = "+id);
            while (DB.result.next()) {
                Integer branch_id = DB.result.getInt("id");
                b = new Branch();
                b.setId(branch_id);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        return b;
    }
    

    public static ArrayList<Branch> all() {

        ArrayList<Branch> branches = new ArrayList<Branch>();
        try {
            DB.query("SELECT * FROM branches INNER JOIN users ON branches.manager_id = users.userID");
            while (DB.result.next()) {
                Integer id = DB.result.getInt("id");
                String name = DB.result.getString("name");
                String address = DB.result.getString("address");
                String phone = DB.result.getString("phone");
                String manager = DB.result.getString("users.first_name") + " " + DB.result.getString("users.last_name");
                Integer manager_id = DB.result.getInt("manager_id");
                Branch b = new Branch(
                        id,
                        name,
                        address,
                        phone,
                        manager_id,
                        manager
                );
                branches.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Branch.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DB.close();
        }
        return branches;
    }
    
    public static int update(Branch b){
        try {         
            String sql = "UPDATE branches SET name=?, address=?, phone=?, manager_id=? WHERE id=?";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setString(1, b.getName());
            DB.preparedStatement.setString(2, b.getAddress());
            DB.preparedStatement.setString(3, b.getPhone());
            DB.preparedStatement.setInt(4, b.getManager_id());
            DB.preparedStatement.setInt(5, b.getId());

            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public static int insert(Branch b){
        
//        String fields = "";
//        for (Field field : b.getClass().getDeclaredFields()) {
//            field.setAccessible(true); // You might want to set modifier to public first.
//            String name = field.getName();
//            fields += name+", ";
//            Object value = field.get(name); 
//            if (value != null) {
//                System.out.println(field.getName() + "=" + value);
//            }
//        }
//        fields = fields.replaceAll(", $", "");
        try {         
            String sql = "INSERT INTO branches (name, address, phone, manager_id) VALUES(?,?,?,?)";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setString(1, b.getName());
            DB.preparedStatement.setString(2, b.getAddress());
            DB.preparedStatement.setString(3, b.getPhone());
            DB.preparedStatement.setInt(4, b.getManager_id());

            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public static int delete(int id){
        try {         
            String sql = "DELETE FROM branches WHERE id = ?";
            DB.preparedStatement = DB.getConn().prepareStatement(sql);
            DB.preparedStatement.setInt(1, id);

            int insert = DB.preparedStatement.executeUpdate();
            DB.close();
            return insert;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}